#!/bin/bash

#
# This script requires two parameter:
#   mode: is one of live | demo (or anything but live)
#         - live will install the real data
#         - demo (or anything else or nothing at all) will install the demo data
#   user: the linux username of the account where the file with live data is located
#         so please make sure the file is there
#

SCRIPT_DIR=$(cd `dirname $0` && pwd)
cd $SCRIPT_DIR
cd ../../../
DRUPAL_DIR=$(pwd)

if [ "$1" == "live" ] ; then
  # Rename the file with demo data
  mv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv.backup
  # Copy the file with live data
  cp /home/$2/RealData.csv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
fi

# Make sure the right modules are enabled and the right migration classes are registered
drush dis -y openaidmap_migrate
drush en -y openaidmap_migrate

# Migrate vocabulary: Organistaion Role
echo "Migrating all Organistaion Role terms"
drush mi OAMTermOrgRoleorganisationrole
echo "Migrate finished."

# Migrate vocabulary: Admin Boundaries
echo "Migrating top level Admin Boundaries terms"
drush mi OAMTermAdminAreasLevel0demodata
echo "Migrate finished."
echo "Migrating all first level Admin Boundaries terms"
drush mi OAMTermAdminAreasLevel1demodata
echo "Migrate finished."
echo "Migrating all second level Admin Boundaries terms"
drush mi OAMTermAdminAreasLevel2demodata
echo "Migrate finished."
echo "Migrating all third level Admin Boundaries terms"
drush mi OAMTermAdminAreasLevel3demodata
echo "Migrate finished."

# Migrate vocabulary: Sectors
echo "Rolling back all top level IATI Sector Codes terms"
drush mi OAMTermSectorsLevel0demodata
echo "Migrate finished."
echo "Rolling back all first level IATI Sector Codes terms"
drush mi OAMTermSectorsLevel1demodata
echo "Migrate finished."
echo "Rolling back all second level IATI Sector Codes terms"
drush mi OAMTermSectorsLevel2demodata
echo "Migrate finished."

# Migrate field: Activity Status
echo "Migrating all Activity Status terms"
drush mi OAMTermActivityStatusdemodata
echo "Migrate finished."

# Migrate entity: Locations
echo "Migrating all Location entities"
drush mi OAMEntityLocationdemodata
echo "Migrate finished."

# Migrate entity: Budgets
echo "Migrating all Budget entities"
drush mi OAMEntityBudgetdemodata
echo "Migrate finished."

# Migrate content: Organisations
echo "Migrating all Funding Organisations content"
drush mi OAMNodeOrgFundingdemodata
echo "Migrate finished."
echo "Migrating all Implementing Organisations content"
drush mi OAMNodeOrgImplementingdemodata
echo "Migrate finished."

# Migrate content: Activities
echo "Migrating all Activities content"
drush mi OAMNodeActivitydemodata
echo "Migrate finished."

# Migrate content: ActivityExtras
echo "Migrating all ActivityExtras content"
drush mi OAMNodeActivityExtrasdemodata
echo "Migrate finished."

# Migrate relation: Organisation Roles
echo "Migrating all Funding Organisations relations"
drush mi OAMRelationOrgRoleFundingdemodata
echo "Migrate finished."
echo "Migrating all Funding Organisations relations"
drush mi OAMRelationOrgRoleImplementingdemodata
echo "Migrate finished."



# Clean up
if [ "$1" == "live" ] ; then
  # Remove the file with live data
  rm $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
  # Restore the file with demo data
  mv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv.backup $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
fi

