<?php
/**
 * @file
 * openaidmap_locations_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function openaidmap_locations_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'openaidmap_locations_context';
  $context->description = '';
  $context->tag = 'OpenAidMap';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'openaidmap_locations_clustered:page' => 'openaidmap_locations_clustered:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-map-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-map-menu',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'facetapi-JOI00XpRmedyM620UVQ8OgDQYFKYBbL1' => array(
          'module' => 'facetapi',
          'delta' => 'JOI00XpRmedyM620UVQ8OgDQYFKYBbL1',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
        'facetapi-A9FRHnCLTgJE1gldKgzPRtQaqzRVUkSH' => array(
          'module' => 'facetapi',
          'delta' => 'A9FRHnCLTgJE1gldKgzPRtQaqzRVUkSH',
          'region' => 'postscript_second',
          'weight' => '-9',
        ),
        'facetapi_graphs-4a41Naqfm3Wfjj8L6CJfT9yJEbKsGx5z' => array(
          'module' => 'facetapi_graphs',
          'delta' => '4a41Naqfm3Wfjj8L6CJfT9yJEbKsGx5z',
          'region' => 'postscript_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('OpenAidMap');
  $export['openaidmap_locations_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'openaidmap_locations_map_context';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'openaidmap_locations_clustered' => 'openaidmap_locations_clustered',
      ),
    ),
    'views_modes' => array(
      'values' => array(
        'default' => 'default',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'openlayers_plus-blockswitcher' => array(
          'module' => 'openlayers_plus',
          'delta' => 'blockswitcher',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['openaidmap_locations_map_context'] = $context;

  return $export;
}
