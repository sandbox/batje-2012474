<?php

class OAMTermSectorsLevel1 extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports the second level sector codes.');
//    $this->dependencies = array('OAMTermSectorsLevel0');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array(
      'parent' => 'parent',
    );
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('iati_activity_sector');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sector_subsector' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'sector_subsector',
          'alias' => 'tdc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'sector_subsector');
    $this->addFieldMapping('parent', 'parent');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermSectorsLevel1' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    if (!empty($row->sector_subsector) && !empty($row->sector)) {
      $term = db_select(str_replace('1', '0', $this->getMap()->getMapTable()), 'mt')
        ->fields('mt', array('destid1'))
        ->condition('sourceid1', $row->sector)
        ->execute()
        ->fetchAssoc();
      if ($term === FALSE) {
        drupal_set_message('Could not find the top level sectors. Please load them before loading the sub-sectors.');
        return FALSE;
      }
      $row->parent = $term['destid1'];
    }
    else {
      drupal_set_message('There is no sub-sector provided.');
      return FALSE;
    }
  }
}
