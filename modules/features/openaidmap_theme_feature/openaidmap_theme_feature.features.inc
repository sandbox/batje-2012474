<?php
/**
 * @file
 * openaidmap_theme_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openaidmap_theme_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "delta" && $api == "delta") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fontyourface_features_default_font().
 */
function openaidmap_theme_feature_fontyourface_features_default_font() {
  return array(
    'Gotham-Bold normal bold' => array(
      'name' => 'Gotham-Bold normal bold',
      'enabled' => 1,
      'url' => 'http://localhost/#12167b070ffab60e669845c93b93f24d',
      'provider' => 'local_fonts',
      'css_selector' => 'h1, h2, h3, h4, h5, h6',
      'css_family' => 'Gotham-Bold',
      'css_style' => 'normal',
      'css_weight' => 'bold',
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:72:"public://fontyourface/local_fonts/Gotham_Bold-normal-bold/stylesheet.css";s:8:"font_uri";a:3:{s:3:"eot";s:70:"public://fontyourface/local_fonts/Gotham_Bold-normal-bold/GothaBol.eot";s:8:"truetype";s:70:"public://fontyourface/local_fonts/Gotham_Bold-normal-bold/GothaBol.ttf";s:4:"woff";s:71:"public://fontyourface/local_fonts/Gotham_Bold-normal-bold/GothaBol.woff";}}',
    ),
    'Gotham-Book normal normal' => array(
      'name' => 'Gotham-Book normal normal',
      'enabled' => 1,
      'url' => 'http://localhost/#1ef635d89de2d03c04055911f34e594d',
      'provider' => 'local_fonts',
      'css_selector' => 'body',
      'css_family' => 'Gotham-Book',
      'css_style' => 'normal',
      'css_weight' => 'normal',
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:74:"public://fontyourface/local_fonts/Gotham_Book-normal-normal/stylesheet.css";s:8:"font_uri";a:3:{s:3:"eot";s:72:"public://fontyourface/local_fonts/Gotham_Book-normal-normal/GothaBoo.eot";s:8:"truetype";s:72:"public://fontyourface/local_fonts/Gotham_Book-normal-normal/GothaBoo.ttf";s:4:"woff";s:73:"public://fontyourface/local_fonts/Gotham_Book-normal-normal/GothaBoo.woff";}}',
    ),
    'Gotham-Light normal normal' => array(
      'name' => 'Gotham-Light normal normal',
      'enabled' => 1,
      'url' => 'http://localhost/#2bf3dd46198985ca0d1a6ddc0203011a',
      'provider' => 'local_fonts',
      'css_selector' => NULL,
      'css_family' => 'Gotham-Light',
      'css_style' => 'normal',
      'css_weight' => 'normal',
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:75:"public://fontyourface/local_fonts/Gotham_Light-normal-normal/stylesheet.css";s:8:"font_uri";a:3:{s:3:"eot";s:73:"public://fontyourface/local_fonts/Gotham_Light-normal-normal/GothaLig.eot";s:8:"truetype";s:73:"public://fontyourface/local_fonts/Gotham_Light-normal-normal/GothaLig.ttf";s:4:"woff";s:74:"public://fontyourface/local_fonts/Gotham_Light-normal-normal/GothaLig.woff";}}',
    ),
    'Gotham-Medium normal normal' => array(
      'name' => 'Gotham-Medium normal normal',
      'enabled' => 1,
      'url' => 'http://localhost/#59e7f3bbc9c618322aa142d3549081bb',
      'provider' => 'local_fonts',
      'css_selector' => NULL,
      'css_family' => 'Gotham-Medium',
      'css_style' => 'normal',
      'css_weight' => 'normal',
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:76:"public://fontyourface/local_fonts/Gotham_Medium-normal-normal/stylesheet.css";s:8:"font_uri";a:3:{s:3:"eot";s:74:"public://fontyourface/local_fonts/Gotham_Medium-normal-normal/GothaMed.eot";s:8:"truetype";s:74:"public://fontyourface/local_fonts/Gotham_Medium-normal-normal/GothaMed.ttf";s:4:"woff";s:75:"public://fontyourface/local_fonts/Gotham_Medium-normal-normal/GothaMed.woff";}}',
    ),
  );
}
