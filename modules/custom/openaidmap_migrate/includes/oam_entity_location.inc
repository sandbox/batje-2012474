<?php

class OAMEntityLocation extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports location entities.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array(
      'geo_type' => 'geo_type',
      'input_format' => 'input_format',
    );
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
      
    // Instantiate the destination class using the entity type.
    $this->destination = new MigrateDestinationEntityAPI('iati_location', 'iati_location');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ter_municipio_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'ter_municipio_codigo',
          'alias' => 'tmc',
        )
      ),
      MigrateDestinationEntityAPI::getKeySchema('iati_location')
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'ter_municipio');
    $this->addFieldMapping('description', 'ter_municipio_codigo');

    $geo_arguments = array(
      'lat' => array('source_field' => 'Latitud'),
      'lon' => array('source_field' => 'Longitud'),
      'geo_type' => array('source_field', 'geo_type'),
      'input_format' => array('source_field' => 'input_format'),
    );
    // The geometry type should be passed in as the primary value.
    $this->addFieldMapping('field_iati_geofield', 'geo_type')->arguments($geo_arguments);
    // Since the excerpt is mapped via an argument, add a null mapping so it's
    // not flagged as unmapped.
//    $this->addFieldMapping(NULL, 'input_format');
//    $this->addFieldMapping(NULL, 'geo_type');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMEntityLocation' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    $row->geo_type = 'point';
    $row->input_format = 'lat/lon';
  }

  public function prepare($entity, $source_row) {
    $adminareamaptable = str_replace('oamentitylocation', 'oamtermadminareaslevel3', $this->getMap()->getMapTable());
    $adminareas = db_select($adminareamaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->ter_municipio_codigo)
      ->execute()
      ->fetchAssoc();
    if ($adminareas === FALSE) {
      drupal_set_message('You first have to load the administrative areas.');
      return FALSE;
    }
    $entity->iati_admin_boundaries[LANGUAGE_NONE][0]['tid'] = $adminareas['destid1'];
  }
}
