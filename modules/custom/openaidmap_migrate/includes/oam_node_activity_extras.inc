<?php

class OAMNodeActivityExtras extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports admin areas, locations and budgets on activities.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    $this->destination = new MigrateDestinationNode('iati_activity');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'codigo_sisin' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'codigo_sisin',
          'alias' => 'cs',
        ),
        'ter_municipio_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'ter_municipio_codigo',
          'alias' => 'tmc',
        ),
        'organismo_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'organismo_codigo',
          'alias' => 'oc',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('title', 'proyecto_nombre');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMNodeActivityExtras' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepare($entity, $source_row) {
    $activitymaptable = str_replace('oamnodeactivityextras', 'oamnodeactivity', $this->getMap()->getMapTable());
    $activities = db_select($activitymaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->codigo_sisin)
      ->execute()
      ->fetchAssoc();
    if ($activities === FALSE) {
      drupal_set_message('You first have to load the acivities.');
      return FALSE;
    }

    $adminareamaptable = str_replace('oamnodeactivityextras', 'oamtermadminareaslevel3', $this->getMap()->getMapTable());
    $adminareas = db_select($adminareamaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->ter_municipio_codigo)
      ->execute()
      ->fetchAssoc();
    if ($adminareas === FALSE) {
      drupal_set_message('You first have to load the administrative areas.');
      return FALSE;
    }

    $locationmaptable = str_replace('oamnodeactivityextras', 'oamentitylocation', $this->getMap()->getMapTable());
    $locations = db_select($locationmaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->ter_municipio_codigo)
      ->execute()
      ->fetchAssoc();
    if ($locations === FALSE) {
      drupal_set_message('You first have to load the locations.');
      return FALSE;
    }

    $source_row->source_type = 'entity';
    $budgetmaptable = str_replace('oamnodeactivityextras', 'oamentitybudget', $this->getMap()->getMapTable());
    $budgets = db_select($budgetmaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->codigo_sisin)
      ->execute()
      ->fetchAssoc();
    if ($budgets === FALSE) {
      drupal_set_message('You first have to load the budgets.');
      return FALSE;
    }

    $activityextrasmaptable = $this->getMap()->getMapTable();
    $adminareas = db_select($activityextrasmaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->codigo_sisin)
//      ->condition('sourceid2', $source_row->ter_municipio_codigo)
      ->execute();

    if ($adminareas !== FALSE) {
      $delta = 0;
      while ($adminarea = $adminareas->fetchAssoc()) {
        $entity->iati_admin_boundaries[LANGUAGE_NONE][$delta]['tid'] = $adminarea['destid1'];
        $entity->field_iati_location[LANGUAGE_NONE][$delta]['target_id'] = $locations['destid1'];
        $delta++;
      }
    }

    $budgets = db_select($activityextrasmaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->codigo_sisin)
//      ->condition('sourceid2', $source_row->ter_municipio_codigo)
//      ->condition('sourceid3', $source_row->organismo_codigo)
      ->execute();
    if ($budgets !== FALSE) {
      $delta = 0;
      while ($budget = $budgets->fetchAssoc()) {
        $entity->field_iati_activity_budget[LANGUAGE_NONE][$delta]['target_id'] = $budget['destid1'];
        $delta++;
      }
    }

    // Don'create a new activity but overwrite the existing one.
    $entity->is_new = FALSE;
    $entity->nid = $activities['destid1'];
  }
}
