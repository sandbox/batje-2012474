<?php
/**
 * @file
 * openaidmap_activities_feature.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function openaidmap_activities_feature_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'openlayers_source_external';
  $strongarm->value = 'http://openlayers.org/api/2.12/OpenLayers.js';
  $export['openlayers_source_external'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'openlayers_source_internal_variant';
  $strongarm->value = 'original';
  $export['openlayers_source_internal_variant'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'openlayers_source_type';
  $strongarm->value = 'internal';
  $export['openlayers_source_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'purl_method_search_api_key';
  $strongarm->value = array(
    'openaidmap_activities' => 'openaidmap_activities',
    'openaidmap_activities:page' => 'openaidmap_activities:page',
    'openaidmap_miscellaneous' => 'openaidmap_miscellaneous',
  );
  $export['purl_method_search_api_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'purl_method_views_modes';
  $strongarm->value = 'querystring';
  $export['purl_method_views_modes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'purl_method_views_modes_key';
  $strongarm->value = 'display';
  $export['purl_method_views_modes_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'purl_types';
  $strongarm->value = array(
    'querystring' => 'querystring',
    'purl_search_api' => 'purl_search_api',
    'path' => 0,
    'pair' => 0,
    'subdomain' => 0,
    'domain' => 0,
    'extension' => 0,
    'useragent' => 0,
  );
  $export['purl_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'locations';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
