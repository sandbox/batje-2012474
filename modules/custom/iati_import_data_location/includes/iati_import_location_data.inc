<?php 
/**
 * @file.
 * Import the data for the organisation.
 */
class LatLongLocationData extends XMLMigration {
  /**
   * Call to the class constructor.
   */
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();
    // Do some general administration.
    $this->description = t('Imports data for latitude and longitude.');

    // Instantiate the map.
     $fields = array(
      'name' => 'name',
      'latitude' => 'latitude',
      'longitude' => 'longitude',
      'country_code' => 'country_code',
      'activity' => 'activity',
    );
//    $items_url = 'http://foreignassistance.gov/IATI/Activities/Nepal/Nepal.xml';
    $item_xpath = '/iati-activities/iati-activity/location';  // relative to document
    $item_ID_xpath = 'administrative/@country';          // relative to item_xpath
    $this->source = new MigrateSourceXML($arguments['source_file'], $item_xpath, $item_ID_xpath, $fields);
    $this->destination = new MigrateDestinationEntityAPI('iati_location', 'iati_location');
    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
    array(
      'country_code' => array(
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'description' => 'country_code',
        'alias' => 'c',
      ),
    ),
    MigrateDestinationEntityAPI::getKeySchema('iati_location')
    );
    $this->addFieldMapping('name', 'name')->xpath('administrative');
    $this->addFieldMapping(NULL, 'latitude')->xpath('coordinates/@latitude');
    $this->addFieldMapping(NULL, 'longitude')->xpath('coordinates/@longitude');
    $this->addFieldMapping(NULL, 'country_code')->xpath('administrative/@country');
    $this->addFieldMapping(NULL, 'activity')->xpath('aparent::iati-activity/iati-identifier');
    $geo_arguments = array(
      'lat' => array('source_field' => 'latitude'),
      'lon' => array('source_field' => 'longitude'),
      'geo_type' => array('source_field', 'geo_type'),
      'input_format' => array('source_field' => 'input_format'),
    );
    // The geometry type should be passed in as the primary value.
    $this->addFieldMapping('field_iati_geofield', 'geo_type')->arguments($geo_arguments);
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'latlonglocationdata' . str_replace('-', '', drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME)));
  }

  public function prepareRow($row) {
  	$row->geo_type = 'point';
  	$row->input_format = 'lat/lon';
  }
  
  public function prepare($entity, $source_row) {
  	$source_row->source_type = 'term';
  	$adminareamaptable = str_replace('latlonglocationdata' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME)), 'codelistlocationlevel1', $this->getMap()->getMapTable());
  	$adminareas = db_select($adminareamaptable, 'mt')
  	->fields('mt', array('sourceid1', 'destid1'))
  	->condition('sourceid1', $source_row->country_code)
  	->execute()
  	->fetchAssoc();
  	if ($adminareas === FALSE) {
  		drupal_set_message('You first have to load the administrative areas.');
  		return FALSE;
  	}
  	$entity->iati_admin_boundaries['en'][0]['tid'] = $adminareas['destid1'];

  	$activitymaptable = str_replace('latlonglocationdata', 'dataactivity', $this->getMap()->getMapTable());
  	$activities = db_select($activitymaptable, 'mt')
      ->fields('mt', array('sourceid1', 'destid1'))
      ->condition('sourceid1', $source_row->activity)
      ->execute()
      ->fetchAssoc();
  	if ($activities === FALSE) {
      drupal_set_message('You first have to load data for activities.');
      return FALSE;
  	}
  	$entity->field_iati_activity_ref['und'][0]['target_id'] = $activities['destid1'];
  }

}
