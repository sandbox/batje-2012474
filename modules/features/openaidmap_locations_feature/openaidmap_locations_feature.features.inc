<?php
/**
 * @file
 * openaidmap_locations_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openaidmap_locations_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_layers") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function openaidmap_locations_feature_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function openaidmap_locations_feature_default_search_api_index() {
  $items = array();
  $items['openaidmap_locations_clustered_index'] = entity_import('search_api_index', '{
    "name" : "openaidmap_locations_clustered_index",
    "machine_name" : "openaidmap_locations_clustered_index",
    "description" : null,
    "server" : "openaidmap_clustered_server",
    "item_type" : "iati_location",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "lid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "field_iati_geofield:lat" : { "type" : "decimal" },
        "field_iati_geofield:lon" : { "type" : "decimal" },
        "field_iati_geofield:latlon" : { "type" : "string", "real_type" : "location" },
        "field_iati_geofield:geocluster_index_12" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_11" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_10" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_9" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_8" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_7" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_6" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_5" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_4" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_3" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_2" : { "type" : "string" },
        "field_iati_geofield:geocluster_index_1" : { "type" : "string" },
        "field_iati_activity_ref:nid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_iati_activity_ref:field_iati_activity_budget" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "iati_budget"
        },
        "field_iati_activity_ref:iati_activity_sector" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_iati_activity_ref:iati_admin_boundaries" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_iati_activity_ref:field_iati_activity_budget:value_amount" : { "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E" }
      },
      "data_alter_callbacks" : {
        "geocluster_solr_fields_alter" : {
          "status" : 1,
          "weight" : "-50",
          "settings" : { "fields" : {
              "field_iati_activity_ref:nid" : "field_iati_activity_ref:nid",
              "field_iati_activity_ref:field_iati_activity_budget:value_amount" : "field_iati_activity_ref:field_iati_activity_budget:value_amount",
              "field_iati_activity_ref:field_iati_activity_budget" : 0,
              "field_iati_activity_ref:iati_activity_sector" : 0,
              "field_iati_activity_ref:iati_admin_boundaries" : 0
            }
          }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : [],
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : { "fields" : [], "spaces" : "[^[:alnum:]]", "ignorable" : "[\\u0027]" }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : [],
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['openaidmap_locations_index'] = entity_import('search_api_index', '{
    "name" : "openaidmap_locations_index",
    "machine_name" : "openaidmap_locations_index",
    "description" : null,
    "server" : "openaidmap_solrserver",
    "item_type" : "iati_location",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "lid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "field_iati_activity_ref:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_iati_activity_ref:iati_activity_sector" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_iati_activity_ref:iati_admin_boundaries" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function openaidmap_locations_feature_default_search_api_server() {
  $items = array();
  $items['openaidmap_clustered_server'] = entity_import('search_api_server', '{
    "name" : "Solr Clustered Server",
    "machine_name" : "openaidmap_clustered_server",
    "description" : "",
    "class" : "geocluster_solr_service",
    "options" : {
      "scheme" : "http",
      "host" : "localhost",
      "port" : 8080,
      "path" : "\\/solr\\/",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
