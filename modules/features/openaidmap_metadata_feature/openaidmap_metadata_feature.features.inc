<?php
/**
 * @file
 * openaidmap_metadata_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openaidmap_metadata_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_rdf_default_mappings().
 */
function openaidmap_metadata_feature_rdf_default_mappings() {
  $rdf_mappings = array();

  // Exported RDF mapping: iati_organisation
  $rdf_mappings['node']['iati_organisation'] = array(
    'rdftype' => array(
      0 => 'schema:Organization',
      1 => 'sioc:Item',
      2 => 'foaf:Document',
      3 => 'iati:organisation',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'schema:name',
      ),
      'type' => 'property',
      'datatype' => 'xsd:Name',
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'schema:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_langauge' => array(
      'predicates' => array(
        0 => 'schema:inLanguage',
        1 => 'iati:language',
      ),
      'type' => 'rel',
    ),
    'url' => array(
      'predicates' => array(
        0 => 'schema:url',
      ),
      'type' => 'rel',
    ),
    'field_iati_identifier' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'iati:iati-identifier',
      ),
      'datatype' => 'xsd:string',
    ),
    'field_iati_org_description' => array(
      'type' => 'property',
      'datatype' => 'xsd:string',
      'predicates' => array(
        0 => 'schema:summary',
        1 => 'schema:description',
        2 => 'iati:description',
      ),
    ),
    'field_iati_org_type' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:organisation-type',
      ),
    ),
    'field_iati_org_url' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'schema:url',
        1 => 'iati:organisation-website',
        2 => 'iati:website',
      ),
    ),
    'field_org_logo' => array(
      'predicates' => array(
        0 => 'schema:image',
      ),
      'type' => 'rel',
    ),
    'field_iati_default_currency' => array(
      'type' => 'property',
      'datatype' => 'xsd:string',
      'predicates' => array(
        0 => 'schema:currenciesAccepted',
        1 => 'iati:value-currency',
      ),
    ),
  );

  // Exported RDF mapping: iati_activity
  $rdf_mappings['node']['iati_activity'] = array(
    'rdftype' => array(
      0 => 'schema:Action',
      1 => 'sioc:Item',
      2 => 'foaf:Document',
      3 => 'iati:actvity',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'schema:name',
        1 => 'rdfs:label',
      ),
      'type' => 'property',
      'datatype' => 'xsd:Name',
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'schema:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_iati_activity_planned_date' => array(
      'type' => 'property',
      'datatype' => 'xsd:dateTime',
      'predicates' => array(
        0 => 'schema:startDate',
        1 => 'iati:end-planned-date',
      ),
    ),
    'field_iati_activity_budget' => array(
      'type' => 'rev',
      'predicates' => array(
        0 => 'iati:activity-budget',
        1 => 'iati:budget',
      ),
    ),
    'field_other_identifier' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'iati:activity-other-identifier',
      ),
      'datatype' => 'xsd:string',
    ),
    'field_iati_activity_website' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'schema:url',
        1 => 'iati:activity-website',
      ),
    ),
    'url' => array(
      'predicates' => array(
        0 => 'schema:url',
      ),
      'type' => 'rel',
    ),
    'field_iati_activity_description' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'schema:summary',
        1 => 'schema:description',
        2 => 'iati:activity-description',
        3 => 'skos:definition',
      ),
      'datatype' => 'xsd:string',
    ),
    'field_iati_activity_status' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-status',
      ),
    ),
    'field_iati_activity_actual_date' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'iati:start-actual-date',
      ),
      'datatype' => 'xsd:dateTime',
    ),
    'field_iati_identifier' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'iati:activity-id',
      ),
      'datatype' => 'xsd:string',
    ),
    'field_field_iati_activity_logo' => array(
      'predicates' => array(
        0 => 'schema:brand',
      ),
      'type' => 'rel',
    ),
    'field_iati_activity_collabo_type' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-collaboration-type',
      ),
    ),
    'field_iati_activity_aid_type' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-default-aid-type',
      ),
    ),
    'iati_activity_sector' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-sector',
      ),
    ),
    'field_iati_activity_finance_type' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-default-finance-type',
      ),
    ),
    'field_iati_activity_flow_type' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:activity-default-flow-type',
      ),
    ),
    'field_iati_owner_ref' => array(
      'type' => 'property',
      'datatype' => 'xsd:string',
      'predicates' => array(
        0 => 'iati:other-identifier-owner-ref',
      ),
    ),
    'field_iati_owner_name' => array(
      'type' => 'property',
      'predicates' => array(
        0 => 'schema:name',
        1 => 'iati:other-identifier-owner-name',
      ),
      'datatype' => 'xsd:Name',
    ),
    'field_field_iati_activity_doc' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'schema:url',
        1 => 'iati:activity-document-link',
      ),
    ),
    'field_iati_location' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'schema:location',
        1 => 'iati:activity-location',
        2 => 'iati:location',
      ),
    ),
    'field_new_budget' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'iati:budget-type',
        1 => 'iati:budget',
        2 => 'iati:activity-budget',
      ),
    ),
    'field_add_organisation' => array(
      'type' => 'rel',
      'predicates' => array(
        0 => 'schema:Organization',
        1 => 'iati:organisation',
      ),
    ),
  );

  return $rdf_mappings;
}
