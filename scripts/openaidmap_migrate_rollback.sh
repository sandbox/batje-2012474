#!/bin/bash

#
# This script requires two parameter:
#   mode: is one of live | demo (or anything but live)
#         - live will rollback the real data
#         - demo (or anything else or nothing at all) will rollback the demo data
#   user: the linux username of the account where the file with live data is located
#

SCRIPT_DIR=$(cd `dirname $0` && pwd)
cd $SCRIPT_DIR
cd ../../../
DRUPAL_DIR=$(pwd)

if [ "$1" == "live" ] ; then
  # Rename the file with demo data
  mv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv.backup
  # Copy the file with live data, this has to be present, even on a rollback
  cp /home/$2/RealData.csv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
fi

# Rollback relation: Organisation Roles
echo "Rolling back all Implementing Organisations relations"
drush mr OAMRelationOrgRoleImplementingdemodata
echo "Rollback finished."
echo "Rolling back all Funding Organisations relations"
drush mr OAMRelationOrgRoleFundingdemodata
echo "Rollback finished."

# Rollback content: ActivityExtras
echo "Rolling back all ActivityExtras content"
drush mr OAMNodeActivityExtrasdemodata
echo "Rollback finished."

# Rollback content: Activities
echo "Rolling back all Activities content"
drush mr OAMNodeActivitydemodata
echo "Rollback finished."

# Rollback content: Organisations
echo "Rolling back all Funding Organisations content"
drush mr OAMNodeOrgFundingdemodata
echo "Rollback finished."
echo "Rolling back all Implementing Organisations content"
drush mr OAMNodeOrgImplementingdemodata
echo "Rollback finished."

# Rollback entity: Budgets
echo "Rolling back all Budget entities"
drush mr OAMEntityBudgetdemodata
echo "Rollback finished."

# Rollback entity: Locations
echo "Rolling back all Location entities"
drush mr OAMEntityLocationdemodata
echo "Rollback finished."

# Rollback field: Activity Status
echo "Rolling back all Activity Status terms"
drush mr OAMTermActivityStatusdemodata
echo "Rollback finished."

# Rollback vocabulary: Sectors
echo "Rolling back all second level IATI Sector Codes terms"
drush mr OAMTermSectorsLevel2demodata
echo "Rollback finished."
echo "Rolling back all first level IATI Sector Codes terms"
drush mr OAMTermSectorsLevel1demodata
echo "Rollback finished."
echo "Rolling back all top level IATI Sector Codes terms"
drush mr OAMTermSectorsLevel0demodata
echo "Rollback finished."

# Rollback vocabulary: Admin Boundaries
echo "Rolling back all third level Admin Boundary terms"
drush mr OAMTermAdminAreasLevel3demodata
echo "Rollback finished."
echo "Rolling back all second level Admin Boundaries terms"
drush mr OAMTermAdminAreasLevel2demodata
echo "Rollback finished."
echo "Rolling back all first level Admin Boundaries terms"
drush mr OAMTermAdminAreasLevel1demodata
echo "Rollback finished."
echo "Rolling back top level Admin Boundaries terms"
drush mr OAMTermAdminAreasLevel0demodata
echo "Rollback finished."

# Rollback vocabulary: Organisation Role
echo "Rolling back all Organistaion Role terms"
drush mr OAMTermOrgRoleorganisationrole
echo "Rollback finished."

# Clean up
if [ "$1" == "live" ] ; then
  # Remove the file with live data
  rm $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
  # Restore the file with demo data
  mv $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv.backup $DRUPAL_DIR/profiles/openaidmap/modules/custom/openaidmap_migrate/demodata/DemoData.csv
fi

