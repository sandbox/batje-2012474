<?php
/**
 * @file
 * openaidmap_locations_feature.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function openaidmap_locations_feature_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-map-menu.
  $menus['menu-map-menu'] = array(
    'menu_name' => 'menu-map-menu',
    'title' => 'Map Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Map Menu');


  return $menus;
}
