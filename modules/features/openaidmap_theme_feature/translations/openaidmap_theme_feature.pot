# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: openaidmap_theme_feature.info: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2013-07-25 11:27+0300\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: openaidmap_theme_feature.info:0
msgid "OpenAidMap Theme Feature"
msgstr ""

#: openaidmap_theme_feature.info:0
msgid "Feature that contains all configuration of the main feature."
msgstr ""

#: openaidmap_theme_feature.info:0
msgid "OpenAidMap"
msgstr ""

