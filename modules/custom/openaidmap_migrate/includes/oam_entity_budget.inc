<?php

class OAMEntityBudget extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports budget entities.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
      
    // Instantiate the destination class using the entity type.
    $this->destination = new MigrateDestinationEntityAPI('iati_budget', 'iati_budget');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'codigo_sisin' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'codigo_sisin',
          'alias' => 'cs',
        ),
        'ter_municipio_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'ter_municipio_codigo',
          'alias' => 'tmc',
        ),
        'organismo_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'organismo_codigo',
          'alias' => 'oc',
        ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('iati_budget')
    );

    // Instantiate the field mapping.
//    $this->addFieldMapping('name', 'codigo_sisin-ter_municipio_codigo');
    $this->addFieldMapping('value_amount', 'monto_presupuesto_Bs');
//    $this->addFieldMapping('value_date', 'fecha_inicio_estimada');
//    $this->addFieldMapping('period_start', 'fecha_inicio_estimada');
//    $this->addFieldMapping('period_end', 'fecha_fin_estimada');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMEntityBudget' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepare($entity, $source_row) {
    $entity->name = $source_row->codigo_sisin . '-' . $source_row->ter_municipio_codigo;

    $entity->value_date = strtotime($source_row->fecha_inicio_estimada);

    $entity->period_start = strtotime($source_row->fecha_inicio_estimada);
    $entity->period_start_text = date('l, jS F Y', strtotime($source_row->fecha_inicio_estimada));
    $entity->period_end = strtotime($source_row->fecha_fin_estimada);
    $entity->period_end_text = date('l, jS F Y', strtotime($source_row->fecha_fin_estimada));
  }
}
