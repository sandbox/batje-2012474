<?php
/**
 * @file
 * openaidmap_activities_overview_feature.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function openaidmap_activities_overview_feature_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'openaidmap_activities_overview_map';
  $openlayers_maps->title = 'OpenAidMap Activities Overview Map';
  $openlayers_maps->description = 'OpenAidMap Activities Overview';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '200px',
    'image_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/img/',
    'css_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '-64.1601562600668, -17.308687863180648',
        'zoom' => '5',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 0,
        'documentDrag' => 0,
      ),
      'openlayers_plus_behavior_popup' => array(
        'layers' => array(
          'openaidmap_activities_overview_openlayers_1' => 'openaidmap_activities_overview_openlayers_1',
        ),
        'hover' => 1,
      ),
      'openlayers_behavior_zoompanel' => array(),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => array(
          'openaidmap_activities_overview_openlayers_1' => 'openaidmap_activities_overview_openlayers_1',
          'mapbox_world_light' => 0,
        ),
        'point_zoom_level' => '10',
        'zoomtolayer_scale' => '1',
      ),
    ),
    'default_layer' => 'mapbox_natural',
    'layers' => array(
      'mapbox_world_light' => 'mapbox_natural',
      'openaidmap_activities_overview_openlayers_1' => 'openaidmap_activities_overview_openlayers_1',
    ),
    'layer_weight' => array(
      'openaidmap_locations_clustered_amount' => '0',
      'openaidmap_locations_clustered_value' => '0',
      'openaidmap_locations_clustered_dummy' => '0',
      'openaidmap_activities_overview_openlayers_1' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'geofield_formatter' => '0',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_locations_clustered_amount' => '0',
      'openaidmap_locations_clustered_value' => '0',
      'openaidmap_locations_clustered_dummy' => '0',
      'openaidmap_activities_overview_openlayers_1' => 'openaidmap_clustered',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_locations_clustered_amount' => '0',
      'openaidmap_locations_clustered_value' => '0',
      'openaidmap_locations_clustered_dummy' => '0',
      'openaidmap_activities_overview_openlayers_1' => 'openaidmap_clustered_selected',
    ),
    'layer_styles_temporary' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_locations_clustered_amount' => '0',
      'openaidmap_locations_clustered_value' => '0',
      'openaidmap_locations_clustered_dummy' => '0',
      'openaidmap_activities_overview_openlayers_1' => 'openaidmap_clustered_temporary',
    ),
    'layer_activated' => array(
      'openaidmap_activities_overview_openlayers_1' => 'openaidmap_activities_overview_openlayers_1',
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'openaidmap_organisations_overview_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_2' => 0,
      'openaidmap_activities_openlayers_1' => 0,
      'openaidmap_locations_clustered_amount' => 0,
      'openaidmap_locations_clustered_value' => 0,
      'openaidmap_locations_clustered_dummy' => 0,
    ),
    'layer_switcher' => array(
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'openaidmap_organisations_overview_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_2' => 0,
      'openaidmap_activities_openlayers_1' => 0,
      'openaidmap_locations_clustered_amount' => 0,
      'openaidmap_locations_clustered_value' => 0,
      'openaidmap_locations_clustered_dummy' => 0,
    ),
    'projection' => 'EPSG:3857',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'openaidmap_clustered',
      'select' => 'openaidmap_clustered_selected',
      'temporary' => 'openaidmap_clustered_temporary',
    ),
  );
  $export['openaidmap_activities_overview_map'] = $openlayers_maps;

  return $export;
}
