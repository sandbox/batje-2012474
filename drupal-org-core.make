api = 2
core = 7.x
projects[drupal][version] = 7.23

; Patches for Core
; No hook_field_schema_alter() http://drupal.org/node/691932#comment-6635614
projects[drupal][patch][] = http://drupal.org/files/field_d7-backport-for-hook_field_schema_alter_691932_58.patch
projects[drupal][patch][] = https://drupal.org/files/1093420-22.patch
