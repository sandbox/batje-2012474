<?php
/**
 * @file
 * openaidmap_locations_feature.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function openaidmap_locations_feature_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'openaidmap_clustered';
  $openlayers_styles->title = 'Openaidmap Clustered';
  $openlayers_styles->description = 'Style to show clusters on the map';
  $openlayers_styles->data = array(
    'pointRadius' => 15,
    'fillColor' => '#EC7721',
    'strokeColor' => '#FFFFFF',
    'strokeWidth' => 2,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'label' => '${name}',
    'labelAlign' => 'cm',
    'fontColor' => '#FFFFFF',
    'fontSize' => '12px',
    'fontFamily' => 'Gotham-Book',
  );
  $export['openaidmap_clustered'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'openaidmap_clustered_selected';
  $openlayers_styles->title = 'Openaidmap Clustered Selected';
  $openlayers_styles->description = 'Style to show clusters on the map in selected state';
  $openlayers_styles->data = array(
    'pointRadius' => 15,
    'fillColor' => '#EC7721',
    'strokeColor' => '#FFDE01',
    'strokeWidth' => 5,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'label' => '${name}',
    'labelAlign' => 'cm',
    'fontColor' => '#FFFFFF',
    'fontSize' => '12px',
    'fontFamily' => 'Gotham-Book',
  );
  $export['openaidmap_clustered_selected'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'openaidmap_clustered_temporary';
  $openlayers_styles->title = 'Openaidmap Clustered Temporary';
  $openlayers_styles->description = 'Style to show clusters on the map in temporary state';
  $openlayers_styles->data = array(
    'pointRadius' => 18,
    'fillColor' => '#CCCCCC',
    'strokeColor' => '#FFDE01',
    'strokeWidth' => 3,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'label' => '${name}',
    'labelAlign' => 'cm',
    'fontColor' => '#FFFFFF',
    'fontSize' => '12px',
    'fontFamily' => 'Gotham-Book',
  );
  $export['openaidmap_clustered_temporary'] = $openlayers_styles;

  return $export;
}
