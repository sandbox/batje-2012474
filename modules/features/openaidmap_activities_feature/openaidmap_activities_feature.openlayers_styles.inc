<?php
/**
 * @file
 * openaidmap_activities_feature.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function openaidmap_activities_feature_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'clone_of_openaidmap_markers';
  $openlayers_styles->title = 'openaidmap_markers_selected';
  $openlayers_styles->description = 'The default OpenAidMap marker style. Orange dot with a yellow outline.';
  $openlayers_styles->data = array(
    'pointRadius' => 15,
    'fillColor' => '#EC7721',
    'strokeColor' => '#FFDE01',
    'strokeWidth' => 5,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
    'fontColor' => '#FFFFFF',
    'fontFamily' => 'Gotham-Book',
  );
  $export['clone_of_openaidmap_markers'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'openaidmap_markers';
  $openlayers_styles->title = 'openaidmap_markers';
  $openlayers_styles->description = 'The default OpenAidMap marker style. Orange dot with a white outline.';
  $openlayers_styles->data = array(
    'pointRadius' => 15,
    'fillColor' => '#EC7721',
    'strokeColor' => '#FFFFFF',
    'strokeWidth' => 2,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
    'fontColor' => '#FFFFFF',
    'fontFamily' => 'Gotham-Book',
  );
  $export['openaidmap_markers'] = $openlayers_styles;

  return $export;
}
