<?php
/**
 * @file
 * openaidmap_organisation_overview_feature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function openaidmap_organisation_overview_feature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|iati_organisation|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'iati_organisation';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'activity_locations' => array(
      'weight' => '1',
      'label' => 'above',
      'format' => 'default',
    ),
    'activity_sectors' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|iati_organisation|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function openaidmap_organisation_overview_feature_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'activity_locations';
  $ds_field->label = 'Activity Locations';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'relation' => 'relation',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|be12edff0a52dc591dc0180aad24488e',
    'block_render' => '3',
  );
  $export['activity_locations'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'activity_sectors';
  $ds_field->label = 'Activity Sectors';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'relation' => 'relation',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|c2edafc1debd695ce5686cabf1d49708',
    'block_render' => '3',
  );
  $export['activity_sectors'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function openaidmap_organisation_overview_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|iati_organisation|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'iati_organisation';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_iati_identifier',
        1 => 'activity_locations',
        2 => 'field_total_budget',
        3 => 'field_recipient_country_budget',
        4 => 'field_recipient_org_budget',
      ),
      'right' => array(
        5 => 'activity_sectors',
      ),
    ),
    'fields' => array(
      'field_iati_identifier' => 'left',
      'activity_locations' => 'left',
      'field_total_budget' => 'left',
      'field_recipient_country_budget' => 'left',
      'field_recipient_org_budget' => 'left',
      'activity_sectors' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|iati_organisation|full'] = $ds_layout;

  return $export;
}
