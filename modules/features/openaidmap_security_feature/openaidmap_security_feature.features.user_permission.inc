<?php
/**
 * @file
 * openaidmap_security_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function openaidmap_security_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access content.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: administer content types.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: administer nodes.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: change own username.
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: create iati_activity content.
  $permissions['create iati_activity content'] = array(
    'name' => 'create iati_activity content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: create iati_organisation content.
  $permissions['create iati_organisation content'] = array(
    'name' => 'create iati_organisation content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any iati_activity content.
  $permissions['delete any iati_activity content'] = array(
    'name' => 'delete any iati_activity content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any iati_organisation content.
  $permissions['delete any iati_organisation content'] = array(
    'name' => 'delete any iati_organisation content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own iati_activity content.
  $permissions['delete own iati_activity content'] = array(
    'name' => 'delete own iati_activity content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own iati_organisation content.
  $permissions['delete own iati_organisation content'] = array(
    'name' => 'delete own iati_organisation content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete terms in 1.
  $permissions['delete terms in 1'] = array(
    'name' => 'delete terms in 1',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 10.
  $permissions['delete terms in 10'] = array(
    'name' => 'delete terms in 10',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 11.
  $permissions['delete terms in 11'] = array(
    'name' => 'delete terms in 11',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 12.
  $permissions['delete terms in 12'] = array(
    'name' => 'delete terms in 12',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 2.
  $permissions['delete terms in 2'] = array(
    'name' => 'delete terms in 2',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 3.
  $permissions['delete terms in 3'] = array(
    'name' => 'delete terms in 3',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 4.
  $permissions['delete terms in 4'] = array(
    'name' => 'delete terms in 4',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 5.
  $permissions['delete terms in 5'] = array(
    'name' => 'delete terms in 5',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 6.
  $permissions['delete terms in 6'] = array(
    'name' => 'delete terms in 6',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 7.
  $permissions['delete terms in 7'] = array(
    'name' => 'delete terms in 7',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 8.
  $permissions['delete terms in 8'] = array(
    'name' => 'delete terms in 8',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 9.
  $permissions['delete terms in 9'] = array(
    'name' => 'delete terms in 9',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit any iati_activity content.
  $permissions['edit any iati_activity content'] = array(
    'name' => 'edit any iati_activity content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any iati_organisation content.
  $permissions['edit any iati_organisation content'] = array(
    'name' => 'edit any iati_organisation content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own iati_activity content.
  $permissions['edit own iati_activity content'] = array(
    'name' => 'edit own iati_activity content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own iati_organisation content.
  $permissions['edit own iati_organisation content'] = array(
    'name' => 'edit own iati_organisation content',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit terms in 1.
  $permissions['edit terms in 1'] = array(
    'name' => 'edit terms in 1',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 10.
  $permissions['edit terms in 10'] = array(
    'name' => 'edit terms in 10',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 11.
  $permissions['edit terms in 11'] = array(
    'name' => 'edit terms in 11',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 12.
  $permissions['edit terms in 12'] = array(
    'name' => 'edit terms in 12',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 2.
  $permissions['edit terms in 2'] = array(
    'name' => 'edit terms in 2',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 3.
  $permissions['edit terms in 3'] = array(
    'name' => 'edit terms in 3',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 4.
  $permissions['edit terms in 4'] = array(
    'name' => 'edit terms in 4',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 5.
  $permissions['edit terms in 5'] = array(
    'name' => 'edit terms in 5',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 6.
  $permissions['edit terms in 6'] = array(
    'name' => 'edit terms in 6',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 7.
  $permissions['edit terms in 7'] = array(
    'name' => 'edit terms in 7',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 8.
  $permissions['edit terms in 8'] = array(
    'name' => 'edit terms in 8',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 9.
  $permissions['edit terms in 9'] = array(
    'name' => 'edit terms in 9',
    'roles' => array(
      'IATI administrator' => 'IATI administrator',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: view own unpublished content.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
