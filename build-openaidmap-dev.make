api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
;includes[] = drupal-org-core.make
projects[drupal][version] = 7.23
; Patches for Core
projects[drupal][patch][] = https://drupal.org/files/1093420-22.patch

; Pull the project from git
projects[openaidmap][type] = "profile"
projects[openaidmap][download][type] = "git"
projects[openaidmap][download][branch] = "7.x-1.x-dev"
projects[openaidmap][download][url]  = "http://git.drupal.org/sandbox/batje/2012474.git"
