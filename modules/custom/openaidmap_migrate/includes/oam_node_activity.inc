<?php

class OAMNodeActivity extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports activities.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    $this->destination = new MigrateDestinationNode('iati_activity');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'codigo_sisin' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'codigo_sisin',
          'alias' => 'cs',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'proyecto_nombre');
    $this->addFieldMapping('field_other_identifier', 'codigo_sisin');
    $this->addFieldMapping('field_iati_activity_description', 'descripcion_solucion');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMNodeActivity' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepare($entity, $source_row) {
    $activitystatusmaptable = str_replace('oamnodeactivity', 'oamtermactivitystatus', $this->getMap()->getMapTable());
    $statuses = db_select($activitystatusmaptable, 'mt')
    ->fields('mt', array('destid1'))
    ->condition('sourceid1', $source_row->etapa_abrev)
    ->execute()
    ->fetchAssoc();
    if ($statuses === FALSE) {
      drupal_set_message('You first have to load the activity statuses.');
      return FALSE;
    }

    $sectormaptable = str_replace('oamnodeactivity', 'oamtermsectorslevel2', $this->getMap()->getMapTable());
    $sectors = db_select($sectormaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $source_row->sector_tipo_proy)
      ->execute()
      ->fetchAssoc();
    if ($sectors === FALSE) {
      drupal_set_message('You first have to load the sectors.');
      return FALSE;
    }

//    $entity->field_iati_activity_description[LANGUAGE_NONE][0]['value'] = $source_row->descripcion_solucion . "\n\n" . $source_row->descripcion_problema . "\n\n" . $source_row->objetivo_especifico;

    $entity->field_iati_activity_planned_date[LANGUAGE_NONE][0]['value'] = substr(date('c', strtotime($source_row->fecha_inicio_estimada)), 0, 19);
    $entity->field_iati_activity_planned_date[LANGUAGE_NONE][0]['value2'] = substr(date('c', strtotime($source_row->fecha_fin_estimada)), 0, 19);

    $entity->field_iati_activity_status[LANGUAGE_NONE][0]['tid'] = $statuses['destid1'];
    $entity->iati_activity_sector[LANGUAGE_NONE][0]['tid'] = $sectors['destid1'];
  }
}
