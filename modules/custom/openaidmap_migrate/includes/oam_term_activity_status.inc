<?php

class OAMTermActivityStatus extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports Activity Status.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('activity_status');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'etapa_abrev' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'etapa_abrev',
          'alias' => 'ea',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'etapa');
    $this->addFieldMapping('field_iati_code', 'etapa_abrev');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermActivityStatus' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }
}
