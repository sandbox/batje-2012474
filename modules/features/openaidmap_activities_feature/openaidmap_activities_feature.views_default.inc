<?php
/**
 * @file
 * openaidmap_activities_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openaidmap_activities_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openaidmap_activities';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_openaidmap_activities_index';
  $view->human_name = 'OpenAidMap Activities';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'openaidmap_activitiesmap';
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'NAME OF ACTIVITY';
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Sector */
  $handler->display->display_options['fields']['iati_activity_sector']['id'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['iati_activity_sector']['field'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['label'] = 'SECTOR';
  $handler->display->display_options['fields']['iati_activity_sector']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['iati_activity_sector']['view_mode'] = 'full';
  /* Field: Indexed Node: Relation iati_organisation_role target node */
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['id'] = 'relation_iati_organisation_role_target_node';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['field'] = 'relation_iati_organisation_role_target_node';

  /* Display: Map */
  $handler = $view->new_display('page', 'Map', 'page');
  $handler->display->display_options['path'] = 'activities';

  /* Display: Openaidmap Data Overlay */
  $handler = $view->new_display('openlayers', 'Openaidmap Data Overlay', 'openlayers_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'other_latlon',
    'other_lat' => 'field_iati_geofield_lat',
    'other_lon' => 'field_iati_geofield_lon',
    'wkt' => 'title',
    'other_top' => 'title',
    'other_right' => 'title',
    'other_bottom' => 'title',
    'other_left' => 'title',
    'name_field' => 'title',
    'description_field' => 'rendered_entity',
    'style_field' => '',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Indexed Node: Location */
  $handler->display->display_options['relationships']['field_iati_location']['id'] = 'field_iati_location';
  $handler->display->display_options['relationships']['field_iati_location']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['relationships']['field_iati_location']['field'] = 'field_iati_location';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: IATI Location: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'entity_iati_location';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'field_iati_location';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed Node: Sector */
  $handler->display->display_options['fields']['iati_activity_sector']['id'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['iati_activity_sector']['field'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['label'] = 'SECTOR';
  $handler->display->display_options['fields']['iati_activity_sector']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['iati_activity_sector']['view_mode'] = 'full';
  /* Field: Indexed Node: Relation iati_organisation_role target node */
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['id'] = 'relation_iati_organisation_role_target_node';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['field'] = 'relation_iati_organisation_role_target_node';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['label'] = 'Donors';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['view_mode'] = 'full';
  /* Field: IATI Location: Iati Location » Latitude */
  $handler->display->display_options['fields']['field_iati_geofield_lat']['id'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['table'] = 'entity_iati_location';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['field'] = 'field_iati_geofield_lat';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['relationship'] = 'field_iati_location';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_iati_geofield_lat']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lat']['link_to_entity'] = 0;
  /* Field: IATI Location: Iati Location » Longitude */
  $handler->display->display_options['fields']['field_iati_geofield_lon']['id'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['table'] = 'entity_iati_location';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['field'] = 'field_iati_geofield_lon';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['relationship'] = 'field_iati_location';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['label'] = '';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_iati_geofield_lon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iati_geofield_lon']['precision'] = '0';
  $handler->display->display_options['fields']['field_iati_geofield_lon']['link_to_entity'] = 0;
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'map_popup';

  /* Display: List */
  $handler = $view->new_display('mode', 'List', 'list');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'relation_iati_organisation_role_target_node' => 'relation_iati_organisation_role_target_node',
    'iati_activity_sector' => 'iati_activity_sector',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'relation_iati_organisation_role_target_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'iati_activity_sector' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'NAME OF ACTIVITY';
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Relation iati_organisation_role target node */
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['id'] = 'relation_iati_organisation_role_target_node';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['field'] = 'relation_iati_organisation_role_target_node';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['label'] = 'DONOR';
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['relation_iati_organisation_role_target_node']['view_mode'] = 'full';
  /* Field: Indexed Node: Sector */
  $handler->display->display_options['fields']['iati_activity_sector']['id'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['table'] = 'search_api_index_openaidmap_activities_index';
  $handler->display->display_options['fields']['iati_activity_sector']['field'] = 'iati_activity_sector';
  $handler->display->display_options['fields']['iati_activity_sector']['label'] = 'SECTOR';
  $handler->display->display_options['fields']['iati_activity_sector']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['iati_activity_sector']['view_mode'] = 'full';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
  );
  $handler->display->display_options['mode_id'] = 'list';
  $handler->display->display_options['mode_name'] = 'List';
  $translatables['openaidmap_activities'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('NAME OF ACTIVITY'),
    t('SECTOR'),
    t('Relation iati_organisation_role target node'),
    t('Map'),
    t('Openaidmap Data Overlay'),
    t('Location'),
    t('Donors'),
    t('Iati Location » Latitude'),
    t('.'),
    t(','),
    t('List'),
    t('DONOR'),
  );
  $export['openaidmap_activities'] = $view;

  $view = new view();
  $view->name = 'openaidmap_miscellaneous';
  $view->description = 'This view has small things that still need to be exported. Miscellaneous ';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'OpenAidMap Miscellaneous';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Footer */
  $handler = $view->new_display('block', 'Footer', 'block');
  $handler->display->display_options['display_description'] = 'Some text in the footer';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '© 2013 Open Aid Partnership. Todos los derechos reservados. Legal.';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $translatables['openaidmap_miscellaneous'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Custom text'),
    t('Footer'),
    t('Some text in the footer'),
    t('© 2013 Open Aid Partnership. Todos los derechos reservados. Legal.'),
  );
  $export['openaidmap_miscellaneous'] = $view;

  return $export;
}
