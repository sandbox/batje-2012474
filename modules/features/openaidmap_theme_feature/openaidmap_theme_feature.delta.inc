<?php
/**
 * @file
 * openaidmap_theme_feature.delta.inc
 */

/**
 * Implements hook_delta_default_templates().
 */
function openaidmap_theme_feature_delta_default_templates() {
  $export = array();

  $delta = new stdClass();
  $delta->disabled = FALSE; /* Edit this to true to make a default delta disabled initially */
  $delta->api_version = 3;
  $delta->machine_name = 'list_view';
  $delta->name = 'List View';
  $delta->description = 'Layout for the list view of the main map page';
  $delta->theme = 'eight';
  $delta->mode = 'preserve';
  $delta->parent = '';
  $delta->settings = array(
    'theme_eight_settings' => array(
      'alpha_debug_block_active' => 0,
      'alpha_debug_grid_active' => 0,
      'alpha_zone_content_order' => 1,
    ),
  );
  $export['list_view'] = $delta;

  return $export;
}
