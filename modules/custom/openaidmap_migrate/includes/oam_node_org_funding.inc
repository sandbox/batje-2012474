<?php

class OAMNodeOrgFunding extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports organisations (funding).');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array();
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationNode('iati_organisation');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'organismo_codigo' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'organismo_codigo',
          'alias' => 'oc',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('title', 'organismo');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMNodeOrgFunding' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }
}
