; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.
projects[context][version] = 3
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[ctools][version] = 1.3
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[entity][version] = 1.1
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.0
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"

projects[cer][version] = 2.x-dev
projects[cer][type] = "module"
projects[cer][subdir] = "dev"

projects[relation_add][version] = 1.1
projects[relation_add][type] = "module"
projects[relation_add][subdir] = "contrib"

projects[relation][version] = 1.0-rc4
projects[relation][type] = "module"
projects[relation][subdir] = "patched"
; Expose source and target facets to facetapi
projects[relation][patch][] = "http://drupal.org/files/relation-sources-targets-2038631-7.patch"
; Expose relation to all endpoints so we can use it in view modes
projects[relation][patch][] = "http://drupal.org/files/relation_dummy_field-view-mode-2045743-2.patch"

projects[views][version] = 3.7
projects[views][type] = "module"
projects[views][subdir] = "patched"
; Allow to inject a custom aggregation implementation http://drupal.org/node/1791796
projects[views][patch][] = http://drupal.org/files/views_post_execute_query_hook.patch
; add GROUP_CONCAT aggregate function; and move aggregate function helpers to a query extender? http://drupal.org/node/1362524#comment-6725878
projects[views][patch][] = http://drupal.org/files/1362524-3.views_.group-concat-aggregate.patch

projects[views_infinite_scroll][version] = 1.1
projects[views_infinite_scroll][type] = "module"
projects[views_infinite_scroll][subdir] = "contrib"

projects[features][subdir] = contrib
projects[features][type] = "module"
projects[features][version] = 2.0-rc1

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

; Other contribs.
projects[inline_entity_form][version] = 1.2
projects[inline_entity_form][type] = "module"
projects[inline_entity_form][subdir] = "contrib"

projects[advanced_help][version] = 1.x-dev
projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "dev"

projects[token][version] = 1.5
projects[token][type] = "module"
projects[token][subdir] = "patched"
projects[token][patch][] = "http://drupal.org/files/token-token_asort_tokens-1712336_0.patch"

projects[migrate][version] = 2.5
projects[migrate][type] = "module"
projects[migrate][subdir] = "contrib"

projects[migrate_extras][version] = 2.5
projects[migrate_extras][type] = "module"
projects[migrate_extras][subdir] = "contrib"

projects[date][version] = 2.6
projects[date][type] = "module"
projects[date][subdir] = "contrib"

;Search related modules.
projects[search_api][subdir] = contrib
projects[search_api][type] = "module"
projects[search_api][version] = 1.6

projects[search_api_db][version] = 1.0-rc1
projects[search_api_db][type] = "module"
projects[search_api_db][subdir] = "contrib"

projects[search_api_solr][version] = 1.1
projects[search_api_solr][type] = "module"
projects[search_api_solr][subdir] = "contrib"

projects[search_api_solr_overrides][version] = 1.0-rc1
projects[search_api_solr_overrides][type] = "module"
projects[search_api_solr_overrides][subdir] = "contrib"

projects[search_api_ranges][version] = 1.4
projects[search_api_ranges][type] = "module"
projects[search_api_ranges][subdir] = "contrib"

projects[facetapi][version] = 1.3
projects[facetapi][type] = "module"
projects[facetapi][subdir] = "patched"
projects[facetapi][patch][] = "http://drupal.org/files/facetapi-1616518-13-show-active-term.patch"
projects[facetapi][patch][] = "http://drupal.org/files/1665164-facetapi-override_facet_label-7.patch"

projects[facetapi_field_filter][version] = 1.x-dev
projects[facetapi_field_filter][type] = "module"
projects[facetapi_field_filter][subdir] = "sandbox"
projects[facetapi_field_filter][download][type] = git
projects[facetapi_field_filter][download][url] = "http://git.drupal.org/sandbox/janvandiepen/2055355.git"

projects[search_api_sorts][version] = 1.4
projects[search_api_sorts][type] = "module"
projects[search_api_sorts][subdir] = "contrib"

projects[search_api_swatches][version] = 1
projects[search_api_swatches][type] = "module"
projects[search_api_swatches][subdir] = "dev"
projects[search_api_swatches][download][type] = git
projects[search_api_swatches][download][url] = "http://git.drupal.org/project/search_api_swatches.git"

projects[purl][version] = 1.x-dev
projects[purl][type] = "module"
projects[purl][subdir] = "dev"

projects[purl_search_api][version] = 1.0-beta1
projects[purl_search_api][type] = "module"
projects[purl_search_api][subdir] = "contrib"

projects[views_modes][version] = 1.x-dev
projects[views_modes][type] = "module"
projects[views_modes][subdir] = "dev"

; Graphing

projects[charts_graphs][version] = 2.0
projects[charts_graphs][type] = "module"
projects[charts_graphs][subdir] = "contrib"

projects[views_charts][version] = 1.x-dev
projects[views_charts][type] = "module"
projects[views_charts][subdir] = "dev"

projects[charts_graphs_flot][version] = 1.x-dev
projects[charts_graphs_flot][type] = "module"
projects[charts_graphs_flot][subdir] = "dev"

projects[facetapi_graphs][version] = 1.x-dev
projects[facetapi_graphs][type] = "module"
projects[facetapi_graphs][subdir] = "dev"

projects[flot][version] = 1.x-dev
projects[flot][type] = "module"
projects[flot][subdir] = "dev"

projects[d3][version] = 1.x-dev
projects[d3][type] = "module"
projects[d3][subdir] = "dev"

projects[charts_graphs_d3][version] = 1.x-dev
projects[charts_graphs_d3][type] = "module"
projects[charts_graphs_d3][subdir] = "sandbox"
projects[charts_graphs_d3][download][type] = git
projects[charts_graphs_d3][download][url] = "http://git.drupal.org/sandbox/ssekono/2028575.git"

; UI improvement modules.
projects[module_filter][version] = 1.7
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[link][version] = 1.1
projects[link][type] = "module"
projects[link][subdir] = "patched"
projects[link][patch][] = "http://drupal.org/files/Fixed_title_value_in_link_field_update_instance_undefined-1914286-3.patch"
projects[link][patch][] = "http://drupal.org/files/link-fix-undefined-index-widget-1918850-9.patch"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

; Internationalization
projects[variable][version] = 2.2
projects[variable][type] = "module"
projects[variable][subdir] = "contrib"

projects[i18n][version] = "1.x-dev"
projects[i18n][type] = "module"
projects[i18n][subdir] = "dev"

projects[l10n_update][version] = "1.x-dev"
projects[l10n_update][type] = "module"
projects[l10n_update][subdir] = "dev"

projects[entity_translation][version] = 1.0-beta3
projects[entity_translation][type] = "module"
projects[entity_translation][subdir] = "contrib"

; Base theme.
projects[omega][version] = 3.1
projects[omega][type] = "theme"

projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][subdir] = "contrib"
projects[omega_tools][type] = "module"

projects[delta][version] = 3.0-beta11
projects[delta][subdir] = "contrib"
projects[delta][type] = "module"

projects[fontyourface][version] = 2.8
projects[fontyourface][subdir] = "contrib"
projects[fontyourface][type] = "module"

; Mapping

projects[geocluster][version] = 1.x-dev
projects[geocluster][subdir] = "patched"
projects[geocluster][type] = "module"
projects[geocluster][download][type] = git
projects[geocluster][download][url] = "http://git.drupal.org/project/geocluster.git"
; http://drupal.org/node/2047407#comment-7714263
; patch that makes geocluster compatible with search_api_solr 7.x-1.1 and adds relations support and removes results without late + lon
projects[geocluster][patch][] = http://drupal.org/files/2047407_12_geocluster_relation_and_search_api_solr_update.patch
; patch that adds hardcoded oam fields to the cluster
; This is superseeded by the geocluster_solr_fields module
; projects[geocluster][patch][] = http://drupalcode.org/sandbox/batje/2012474.git/blob_plain/refs/heads/7.x-1.x-dev-2026571:/patches/oam_geocluster_update.patch
; Patch from the geocluster_solr_fields module to allow for merging clusters
projects[geocluster][patch][]  = http://drupalcode.org/sandbox/batje/2058663.git/blob_plain/refs/heads/7.x-1.x:/geocluster.patch

projects[geocluster_solr_fields][version] = 1.x-dev
projects[geocluster_solr_fields][subdir] = "sandbox"
projects[geocluster_solr_fields][type] = "module"
projects[geocluster_solr_fields][download][type] = git
projects[geocluster_solr_fields][download][url] = "http://git.drupal.org/sandbox/batje/2058663.git"

projects[geocluster_openlayers][version] = 1.x-dev
projects[geocluster_openlayers][subdir] = "sandbox"
projects[geocluster_openlayers][type] = "module"
projects[geocluster_openlayers][download][type] = git
projects[geocluster_openlayers][download][url] = "http://git.drupal.org/sandbox/davidserene/2057805.git"

projects[views_geojson][version] = 1.x-dev
projects[views_geojson][subdir] = "patched"
; Add hook views_geojson_render_fields_alter http://drupal.org/node/1799870
;projects[views_geojson][patch][] = http://drupal.org/files/views_geojson_render_fields_alter.patch
; Recent geofield changes require changes http://drupal.org/node/1794848 (well, it seems not anymore)
;projects[views_geojson][patch][] = http://drupal.org/files/views_geojson_geofield_changes.patch
; BBox argument handler fixes http://drupal.org/node/1839554#comment-6726986
;; This seems to be fixed, except for one small issue
;projects[views_geojson][patch][] = http://drupal.org/files/1839554_views_geojson_bbox_argument_handler_fixes_14.patch
; #2060197 new issue: bbox on all views basetables
;projects[views_geojson][patch][] = http://drupal.org/files/bbox_on_all_views.patch

projects[openlayers][version] = 2.0-beta7
projects[openlayers][type] = "module"
projects[openlayers][subdir] = "patched"
; #2056555 patch to add stuff to geojson layers to make them work with geocluster (is committed for beta8)
projects[openlayers][patch][] = http://drupal.org/files/geojson_layer_more_options.patch
projects[openlayers][patch][] = http://drupal.org/files/bbox_geojson_anydisplay.patch
projects[openlayers][patch][] = http://drupal.org/files/2060451-bbox_geojson_anydisplay_4.patch

projects[geofield][version] = 2.0-beta1
projects[geofield][type] = "module"
projects[geofield][subdir] = "patched"
; this was for issue #1942826 that fixed OpenLayers compatibility but that seems to have been fixed
; projects[geofield][patch][] = "http://drupalcode.org/project/geofield.git/patch/7b5c2b6"
; seems to have been committed
; projects[geofield][patch][] = http://drupal.org/files/geofield_devel_generate_fix.patch
; Got the following error after update from alpha2 to beta1:
;   Undefined index: geom
;   File profiles/openaidmap/modules/contrib/geofield/geofield.module, line 293
; Following patch fixes that
projects[geofield][patch][] = https://drupal.org/files/geofield-undefined_index_geom-2075989.patch
	
projects[openlayers_plus][version] = 3.x-dev
projects[openlayers_plus][type] = "module"
projects[openlayers_plus][subdir] = "contrib"

projects[geophp][version] = 1.7
projects[geophp][type] = "module"
projects[geophp][subdir] = "contrib"

projects[proj4js][version] = 1.2
projects[proj4js][type] = "module"
projects[proj4js][subdir] = "contrib"

projects[views_rss][version] = 2.0-rc3
projects[views_rss][type] = "module"
projects[views_rss][subdir] = "contrib"

projects[kml][version] = 1.0-alpha1
projects[kml][type] = "module"
projects[kml][subdir] = "patched"
projects[kml][patch][] = "http://drupal.org/files/kml_blockify.patch" 

projects[views_data_export][version] = 3.0-beta6
projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "patched"
projects[views_data_export][patch][] = "http://drupal.org/files/views_data_export_blockify.patch"

projects[blockify][version] = 1.2
projects[blockify][type] = "module"
projects[blockify][subdir] = "contrib"

; Quick stuff
projects[quicktabs][version] = 3
projects[quicktabs][type] = "module"
projects[quicktabs][subdir] = "contrib"

projects[ds][version] = 2.4
projects[ds][type] = "module"
projects[ds][subdir] = "contrib"

projects[media][version] = 1
projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[admin_menu][version] = 3
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[diff][version] = 3
projects[diff][type] = "module"
projects[diff][subdir] = "contrib"

projects[module_filter][subdir] = contrib
projects[module_filter][type] = "module"
projects[module_filter][version] = 1.7

projects[footable][version] = 1.x-dev
projects[footable][type] = "module"
projects[footable][subdir] = "dev"

projects[jquery_update][subdir] = contrib
projects[jquery_update][type] = "module"
projects[jquery_update][version] = 2.3

; Metadata from issue #2058673

projects[rdfx][version] = 2.0-alpha4
projects[rdfx][type] = "module"
projects[rdfx][subdir] = "contrib"

projects[restws][version] = 2
projects[restws][type] = "module"
projects[restws][subdir] = "contrib"

projects[schemaorg][version] = 1.0-beta3
projects[schemaorg][type] = "module"
projects[schemaorg][subdir] = "contrib"


; Libraries.

libraries[flot][download][type] = "file"
libraries[flot][download][url] = "http://flot.googlecode.com/files/flot-0.7.tar.gz"
libraries[flot][download][sha1] = "68ca8b250d18203ebe67136913e0e1c82bbeecfb"
libraries[flot][destination] = "libraries"

libraries[autopager][download][type] = "file"
libraries[autopager][download][url] = "http://jquery-autopager.googlecode.com/files/jquery.autopager-1.0.0.js"
libraries[autopager][download][sha1] = "3125f05ff4cd2471730f0775ddee03ea141988cd"
libraries[autopager][destination] = "libraries" 

libraries[d3][download][type] = "get"
libraries[d3][download][url] = "http://github.com/mbostock/d3/zipball/master"
libraries[d3][directory_name] = "d3"
libraries[d3][destination] = "libraries"

libraries[ARC2][download][type] = "file"
libraries[ARC2][download][url] = "http://github.com/semsol/arc2/tarball/master"
;libraries[ARC2][download][sha1] = "03254b909b30b8f5514466322f1446c350974497"
libraries[ARC2][destination] = "libraries/arc"

libraries[FooTable][download][type] = "file"
libraries[FooTable][download][url] = "https://github.com/bradvin/FooTable/archive/V2.zip"
libraries[FooTable][directory_name] = "FooTable"
libraries[FooTable][destination] = "libraries"
