<?php
/**
 * @file
 * openaidmap_locations_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openaidmap_locations_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openaidmap_locations_clustered';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_openaidmap_locations_clustered_index';
  $view->human_name = 'openaidmap_locations_clustered';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Locations Clustered Amount Json';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'openaidmap_locations_map';
  /* Field: Indexed IATI Location: Iati Location */
  $handler->display->display_options['fields']['field_iati_geofield']['id'] = 'field_iati_geofield';
  $handler->display->display_options['fields']['field_iati_geofield']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['field_iati_geofield']['field'] = 'field_iati_geofield';
  $handler->display->display_options['fields']['field_iati_geofield']['label'] = '';
  $handler->display->display_options['fields']['field_iati_geofield']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_iati_geofield']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iati_geofield']['click_sort_column'] = 'geom';
  $handler->display->display_options['fields']['field_iati_geofield']['settings'] = array(
    'data' => 'full',
  );
  /* Field: Content: Geocluster lat */
  $handler->display->display_options['fields']['geocluster_lat']['id'] = 'geocluster_lat';
  $handler->display->display_options['fields']['geocluster_lat']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_lat']['field'] = 'geocluster_lat';
  /* Field: Content: Geocluster lon */
  $handler->display->display_options['fields']['geocluster_lon']['id'] = 'geocluster_lon';
  $handler->display->display_options['fields']['geocluster_lon']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_lon']['field'] = 'geocluster_lon';
  /* Field: Content: Geocluster ids (Solr) */
  $handler->display->display_options['fields']['geocluster_ids']['id'] = 'geocluster_ids';
  $handler->display->display_options['fields']['geocluster_ids']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_ids']['field'] = 'geocluster_ids';
  /* Field: Content: Geocluster result count */
  $handler->display->display_options['fields']['geocluster_count']['id'] = 'geocluster_count';
  $handler->display->display_options['fields']['geocluster_count']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_count']['field'] = 'geocluster_count';
  $handler->display->display_options['geocluster_enabled'] = 1;
  $handler->display->display_options['geocluster_options'] = array(
    'algorithm' => 'geocluster_solr',
    'cluster_field' => 'field_iati_geofield',
    'cluster_distance' => '2',
  );

  /* Display: Locations Clustered */
  $handler = $view->new_display('page', 'Locations Clustered', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['path'] = 'locations';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Locations';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Number of Activities */
  $handler = $view->new_display('page', 'Number of Activities', 'amount');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['display_description'] = 'Shows the amount of Activities per location';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'views_geojson';
  $handler->display->display_options['style_options']['data_source']['value'] = 'latlon';
  $handler->display->display_options['style_options']['data_source']['latitude'] = 'geocluster_lat';
  $handler->display->display_options['style_options']['data_source']['longitude'] = 'geocluster_lon';
  $handler->display->display_options['style_options']['data_source']['geofield'] = 'field_iati_geofield';
  $handler->display->display_options['style_options']['data_source']['wkt'] = 'field_iati_geofield';
  $handler->display->display_options['style_options']['data_source']['name_field'] = 'geocluster_count';
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed IATI Location: Iati Location */
  $handler->display->display_options['fields']['field_iati_geofield']['id'] = 'field_iati_geofield';
  $handler->display->display_options['fields']['field_iati_geofield']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['field_iati_geofield']['field'] = 'field_iati_geofield';
  $handler->display->display_options['fields']['field_iati_geofield']['label'] = '';
  $handler->display->display_options['fields']['field_iati_geofield']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_iati_geofield']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iati_geofield']['click_sort_column'] = 'geom';
  $handler->display->display_options['fields']['field_iati_geofield']['settings'] = array(
    'data' => 'full',
  );
  /* Field: Content: Geocluster lat */
  $handler->display->display_options['fields']['geocluster_lat']['id'] = 'geocluster_lat';
  $handler->display->display_options['fields']['geocluster_lat']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_lat']['field'] = 'geocluster_lat';
  /* Field: Content: Geocluster lon */
  $handler->display->display_options['fields']['geocluster_lon']['id'] = 'geocluster_lon';
  $handler->display->display_options['fields']['geocluster_lon']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_lon']['field'] = 'geocluster_lon';
  /* Field: Content: Geocluster ids (Solr) */
  $handler->display->display_options['fields']['geocluster_ids']['id'] = 'geocluster_ids';
  $handler->display->display_options['fields']['geocluster_ids']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_ids']['field'] = 'geocluster_ids';
  /* Field: Content: Geocluster result count */
  $handler->display->display_options['fields']['geocluster_count']['id'] = 'geocluster_count';
  $handler->display->display_options['fields']['geocluster_count']['table'] = 'search_api_index_openaidmap_locations_clustered_index';
  $handler->display->display_options['fields']['geocluster_count']['field'] = 'geocluster_count';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Custom Text';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Custom: Bounding box */
  $handler->display->display_options['arguments']['bbox_argument']['id'] = 'bbox_argument';
  $handler->display->display_options['arguments']['bbox_argument']['table'] = 'views';
  $handler->display->display_options['arguments']['bbox_argument']['field'] = 'bbox_argument';
  $handler->display->display_options['arguments']['bbox_argument']['default_action'] = 'default';
  $handler->display->display_options['arguments']['bbox_argument']['default_argument_type'] = 'querystring';
  $handler->display->display_options['arguments']['bbox_argument']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['bbox_argument']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['bbox_argument']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['geocluster_enabled'] = 1;
  $handler->display->display_options['geocluster_options'] = array(
    'algorithm' => 'geocluster_solr',
    'cluster_field' => 'field_iati_geofield',
    'cluster_distance' => '65',
  );
  $handler->display->display_options['path'] = 'locations/json/amount';

  /* Display: Total Budgets */
  $handler = $view->new_display('page', 'Total Budgets', 'value');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['display_description'] = 'Shows the total Budget Value per location';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'views_geojson';
  $handler->display->display_options['style_options']['data_source']['value'] = 'latlon';
  $handler->display->display_options['style_options']['data_source']['latitude'] = 'geocluster_lat';
  $handler->display->display_options['style_options']['data_source']['longitude'] = 'geocluster_lon';
  $handler->display->display_options['style_options']['data_source']['geofield'] = 'field_iati_geofield';
  $handler->display->display_options['style_options']['data_source']['wkt'] = 'field_iati_geofield';
  $handler->display->display_options['style_options']['data_source']['name_field'] = 'geocluster_value';
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Custom: Bounding box */
  $handler->display->display_options['arguments']['bbox_argument']['id'] = 'bbox_argument';
  $handler->display->display_options['arguments']['bbox_argument']['table'] = 'views';
  $handler->display->display_options['arguments']['bbox_argument']['field'] = 'bbox_argument';
  $handler->display->display_options['arguments']['bbox_argument']['default_action'] = 'default';
  $handler->display->display_options['arguments']['bbox_argument']['default_argument_type'] = 'querystring';
  $handler->display->display_options['arguments']['bbox_argument']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['bbox_argument']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['bbox_argument']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['geocluster_enabled'] = 1;
  $handler->display->display_options['geocluster_options'] = array(
    'algorithm' => 'geocluster_solr',
    'cluster_field' => 'field_iati_geofield',
    'cluster_distance' => '65',
  );
  $handler->display->display_options['path'] = 'locations/json/value';

  /* Display: Dummy Overlay */
  $handler = $view->new_display('openlayers', 'Dummy Overlay', 'dummy');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Dummy Locations Clustered Layer';
  $handler->display->display_options['display_comment'] = 'This layer is supposed to be empty (hence the -1 in the pager).

The purpose of this layer is to be added to the openlayers map, and to be the in-page view that controls the facets. Without this layer, the real json layer would not be able to tell search_api what search is going on and what facets need to be shown.

This layer is an exact copy of the geojson layer, but then empty and invisible to the user.';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '-1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'other_latlon',
    'other_lat' => 'geocluster_lat',
    'other_lon' => 'geocluster_lon',
    'wkt' => 'field_iati_geofield',
    'other_top' => 'field_iati_geofield',
    'other_right' => 'field_iati_geofield',
    'other_bottom' => 'field_iati_geofield',
    'other_left' => 'field_iati_geofield',
    'name_field' => '',
    'description_field' => '',
    'style_field' => '',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['geocluster_enabled'] = 0;
  $handler->display->display_options['geocluster_options'] = array(
    'algorithm' => 'geocluster_mysql',
    'cluster_field' => '',
    'cluster_distance' => '65',
  );
  $translatables['openaidmap_locations_clustered'] = array(
    t('Master'),
    t('Locations Clustered Amount Json'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Geocluster lat'),
    t('.'),
    t(','),
    t('Geocluster lon'),
    t('Geocluster ids (Solr)'),
    t('Geocluster result count'),
    t('Locations Clustered'),
    t('Number of Activities'),
    t('Shows the amount of Activities per location'),
    t('Custom text'),
    t('Custom Text'),
    t('All'),
    t('Total Budgets'),
    t('Shows the total Budget Value per location'),
    t('Dummy Overlay'),
  );
  $export['openaidmap_locations_clustered'] = $view;

  return $export;
}
