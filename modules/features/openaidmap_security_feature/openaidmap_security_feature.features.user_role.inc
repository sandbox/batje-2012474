<?php
/**
 * @file
 * openaidmap_security_feature.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function openaidmap_security_feature_user_default_roles() {
  $roles = array();

  // Exported role: IATI administrator.
  $roles['IATI administrator'] = array(
    'name' => 'IATI administrator',
    'weight' => 3,
  );

  return $roles;
}
