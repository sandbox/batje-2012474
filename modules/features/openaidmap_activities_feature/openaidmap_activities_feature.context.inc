<?php
/**
 * @file
 * openaidmap_activities_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function openaidmap_activities_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'activities_list_page';
  $context->description = 'List views mode of the front page';
  $context->tag = 'OpenAidMap';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'openaidmap_activities' => 'openaidmap_activities',
      ),
    ),
    'views_modes' => array(
      'values' => array(
        'list' => 'list',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'facetapi-Uy0wtOBJGRqwVawnj1F7DxoryazLDhAb' => array(
          'module' => 'facetapi',
          'delta' => 'Uy0wtOBJGRqwVawnj1F7DxoryazLDhAb',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'quicktabs-activity_sectors' => array(
          'module' => 'quicktabs',
          'delta' => 'activity_sectors',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'facetapi-9OwnG0L8l7UfCLC8B6VeeIC3dlSkJgI1' => array(
          'module' => 'facetapi',
          'delta' => '9OwnG0L8l7UfCLC8B6VeeIC3dlSkJgI1',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'facetapi-yCb3im1R13uy9M1PFbGvZ27IADY55oNS' => array(
          'module' => 'facetapi',
          'delta' => 'yCb3im1R13uy9M1PFbGvZ27IADY55oNS',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'views_modes-modes' => array(
          'module' => 'views_modes',
          'delta' => 'modes',
          'region' => 'preface_first',
          'weight' => '-9',
        ),
        'views-openaidmap_miscellaneous-block' => array(
          'module' => 'views',
          'delta' => 'openaidmap_miscellaneous-block',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'list_view',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('List views mode of the front page');
  t('OpenAidMap');
  $export['activities_list_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'activities_map_page';
  $context->description = '';
  $context->tag = 'OpenAidMap';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'openaidmap_activities:page' => 'openaidmap_activities:page',
      ),
    ),
    'views_modes' => array(
      'values' => array(
        'default' => 'default',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'views_modes-modes' => array(
          'module' => 'views_modes',
          'delta' => 'modes',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'quicktabs-activity_sectors' => array(
          'module' => 'quicktabs',
          'delta' => 'activity_sectors',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
        'facetapi-9OwnG0L8l7UfCLC8B6VeeIC3dlSkJgI1' => array(
          'module' => 'facetapi',
          'delta' => '9OwnG0L8l7UfCLC8B6VeeIC3dlSkJgI1',
          'region' => 'postscript_second',
          'weight' => '-7',
        ),
        'openlayers_plus-blockswitcher' => array(
          'module' => 'openlayers_plus',
          'delta' => 'blockswitcher',
          'region' => 'postscript_second',
          'weight' => '-6',
        ),
        'facetapi-yCb3im1R13uy9M1PFbGvZ27IADY55oNS' => array(
          'module' => 'facetapi',
          'delta' => 'yCb3im1R13uy9M1PFbGvZ27IADY55oNS',
          'region' => 'postscript_second',
          'weight' => '-5',
        ),
        'facetapi_graphs-hYmMPwYxg3QHmZ23B9vPU6lJgXxdzyR0' => array(
          'module' => 'facetapi_graphs',
          'delta' => 'hYmMPwYxg3QHmZ23B9vPU6lJgXxdzyR0',
          'region' => 'postscript_second',
          'weight' => '-4',
        ),
        'views-openaidmap_miscellaneous-block' => array(
          'module' => 'views',
          'delta' => 'openaidmap_miscellaneous-block',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'list_view',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('OpenAidMap');
  $export['activities_map_page'] = $context;

  return $export;
}
