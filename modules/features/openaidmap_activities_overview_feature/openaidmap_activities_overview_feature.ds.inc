<?php
/**
 * @file
 * openaidmap_activities_overview_feature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function openaidmap_activities_overview_feature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|iati_activity|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'iati_activity';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'description' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'organisations_roles' => array(
      'weight' => '6',
      'label' => 'above',
      'format' => 'default',
    ),
    'field_iati_identifier' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'ID',
        ),
      ),
    ),
    'field_iati_location' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'Activity Locations',
          'lb-col' => TRUE,
        ),
      ),
    ),
    'iati_activity_sector' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
        ),
      ),
    ),
  );
  $export['node|iati_activity|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function openaidmap_activities_overview_feature_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'description';
  $ds_field->label = 'Description';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|885433eb2befad90cbffd5619643b67f',
    'block_render' => '3',
  );
  $export['description'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'organisations_roles';
  $ds_field->label = 'Organisations & Roles';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|6603a9c4c6ebdb6af8cc933d81db3e00',
    'block_render' => '3',
  );
  $export['organisations_roles'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function openaidmap_activities_overview_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|iati_activity|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'iati_activity';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_iati_identifier',
        1 => 'description',
        2 => 'field_iati_activity_planned_date',
        3 => 'field_iati_location',
        4 => 'field_iati_activity_status',
      ),
      'right' => array(
        5 => 'iati_activity_sector',
        6 => 'organisations_roles',
      ),
    ),
    'fields' => array(
      'field_iati_identifier' => 'left',
      'description' => 'left',
      'field_iati_activity_planned_date' => 'left',
      'field_iati_location' => 'left',
      'field_iati_activity_status' => 'left',
      'iati_activity_sector' => 'right',
      'organisations_roles' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|iati_activity|full'] = $ds_layout;

  return $export;
}
