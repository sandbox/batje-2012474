<?php

class OAMRelationOrgRoleFunding extends DynamicMigration {
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports relations of type funding organisation role.');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $csvcolumns[] = array('source_type', 'source_type');
    $csvcolumns[] = array('source_id', 'source_id');
    $csvcolumns[] = array('destination_type', 'destination_type');
    $csvcolumns[] = array('destination_id', 'destination_id');
    $csvcolumns[] = array('role_tid', 'role_tid');
    $options = array();
    $fields = array();
    // Instantiate the class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);
    
    $this->destination = new MigrateDestinationRelation('iati_organisation_role');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'source_type' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'source_type',
          'alias' => 'st',
        ),
        'source_id' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'source_id',
          'alias' => 'si',
        ),
        'destination_type' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'destination_type',
          'alias' => 'dt',
        ),
        'destination_id' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'destination_id',
          'alias' => 'di',
        ),
        'role_tid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'role_tid',
          'alias' => 'rt',
        ),
      ),
      MigrateDestinationRelation::getKeySchema()
    );
    
    // Instantiate the field mapping. No field mapping required.
    // All values will be populated in functions prepareRow() and prepare().
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMRelationOrgRoleFunding' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    $rolemaptable = 'migrate_map_oamtermorgroleorganisationrole';
    $roles = db_select($rolemaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', 'Funding')
      ->execute()
      ->fetchAssoc();
    if ($roles === FALSE) {
      drupal_set_message('You first have to load the Organisation Role code list.');
      return FALSE;
    }

    $row->source_type = 'node';
    $row->destination_type = 'node';

    $sourcemaptable = str_replace('oamrelationorgrolefunding', 'oamnodeactivity', $this->getMap()->getMapTable());
    $sources = db_select($sourcemaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $row->codigo_sisin)
      ->execute()
      ->fetchAssoc();
    if ($sources === FALSE) {
      drupal_set_message('You first have to load the activities.');
      return FALSE;
    }

    $destinationmaptable = str_replace('oamrelationorgrolefunding', 'oamnodeorgfunding', $this->getMap()->getMapTable());
    $destinations = db_select($destinationmaptable, 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $row->organismo_codigo)
      ->execute()
      ->fetchAssoc();
    if ($destinations === FALSE) {
      drupal_set_message('You first have to load the funding organisations.');
      return FALSE;
    }

    $row->role_tid = $roles['destid1'];
    $row->source_id = $sources['destid1'];
    $row->destination_id = $destinations['destid1'];
  }

  public function prepare(stdClass $relation, stdClass $source_row) {
    $relation->endpoints[LANGUAGE_NONE] = array(
      array(
        'entity_type' => $source_row->source_type,
        'entity_id' => $source_row->source_id,
      ),
      array(
        'entity_type' => $source_row->destination_type,
        'entity_id' => $source_row->destination_id,
      ),
    );
    $relation->field_organisation_role[LANGUAGE_NONE][0]['tid'] = $source_row->role_tid;
  }
}
