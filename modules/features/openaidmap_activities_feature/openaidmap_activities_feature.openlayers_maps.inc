<?php
/**
 * @file
 * openaidmap_activities_feature.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function openaidmap_activities_feature_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'openaidmap_activitiesmap';
  $openlayers_maps->title = 'OpenAidMap ActivitiesMap';
  $openlayers_maps->description = 'Map populated with locations where given Activities take place';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '500px',
    'image_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/img/',
    'css_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '-64.73144531527817, -18.396230121208717',
        'zoom' => '6',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_cluster' => array(
        'clusterlayer' => array(
          'openaidmap_activities_openlayers_1' => 'openaidmap_activities_openlayers_1',
        ),
        'distance' => '5',
        'threshold' => '',
        'display_cluster_numbers' => 1,
        'middle_lower_bound' => '3',
        'middle_upper_bound' => '50',
        'low_color' => '#EC7721',
        'middle_color' => '#EC922A',
        'high_color' => '#ECAD2F',
      ),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 0,
        'documentDrag' => 0,
      ),
      'openlayers_plus_behavior_blockswitcher' => array(
        'enabled' => 0,
        'open' => 0,
        'overlay_style' => 'checkbox',
        'position' => 'nw',
      ),
      'openlayers_plus_behavior_maptext' => array(
        'regions' => array(
          'page_top' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'page_bottom' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'content' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'user_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'user_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'branding' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'menu' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'sidebar_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'sidebar_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'header_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'header_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_third' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'postscript_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'postscript_second' => array(
            'name' => 1,
            'fieldset' => array(
              'toggle' => 1,
              'title' => 'Collapse',
              'collapse' => 1,
            ),
          ),
          'postscript_third' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 1,
              'title' => 'Collapse',
              'collapse' => 1,
            ),
          ),
          'postscript_fourth' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'footer_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'footer_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
        ),
      ),
      'openlayers_plus_behavior_popup' => array(
        'layers' => array(
          'openaidmap_activities_openlayers_1' => 'openaidmap_activities_openlayers_1',
        ),
        'hover' => 1,
      ),
      'openlayers_behavior_zoompanel' => array(),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => array(
          'mapbox_cloudless' => 'mapbox_cloudless',
          'openaidmap_activities_openlayers_1' => 'openaidmap_activities_openlayers_1',
          'mapbox_plain' => 0,
          'mapbox_natural' => 0,
          'mapbox_dark' => 0,
          'mapbox_print' => 0,
        ),
        'point_zoom_level' => '8',
        'zoomtolayer_scale' => '1',
      ),
    ),
    'default_layer' => 'mapbox_cloudless',
    'layers' => array(
      'mapbox_cloudless' => 'mapbox_cloudless',
      'mapbox_plain' => 'mapbox_plain',
      'mapbox_natural' => 'mapbox_natural',
      'mapbox_dark' => 'mapbox_dark',
      'mapbox_print' => 'mapbox_print',
      'openaidmap_activities_openlayers_1' => 'openaidmap_activities_openlayers_1',
    ),
    'layer_weight' => array(
      'openaidmap_activities_openlayers_1' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openlayers_kml_example' => '0',
      'geofield_formatter' => '0',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => 'openaidmap_markers',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => 'clone_of_openaidmap_markers',
    ),
    'layer_styles_temporary' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => 'clone_of_openaidmap_markers',
    ),
    'layer_activated' => array(
      'openaidmap_activities_openlayers_1' => 'openaidmap_activities_openlayers_1',
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
    ),
    'layer_switcher' => array(
      'openaidmap_activities_openlayers_1' => 0,
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
    ),
    'projection' => 'EPSG:3857',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default_marker_black_small',
      'select' => 'default_marker_black_small',
      'temporary' => 'default_marker_black_small',
    ),
  );
  $export['openaidmap_activitiesmap'] = $openlayers_maps;

  return $export;
}
