<?php
/**
 * @file
 * openaidmap_locations_feature.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function openaidmap_locations_feature_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'openaidmap_locations_map';
  $openlayers_maps->title = 'OpenAidMap Locations Map';
  $openlayers_maps->description = '';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '500px',
    'image_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/img/',
    'css_path' => 'profiles/openaidmap/modules/contrib/openlayers/themes/default_dark/style.css',
    'proxy_host' => 'proxy?request=',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '22.06054684948926, 12.983147735519614',
        'zoom' => '3',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_argparser' => array(
        'anchor' => 0,
      ),
      'openlayers_geocluster_behavior_zoom' => array(
        'layers' => array(
          'openaidmap_locations_clustered_amount' => 'openaidmap_locations_clustered_amount',
          'openaidmap_locations_clustered_value' => 'openaidmap_locations_clustered_value',
          'openaidmap_locations_clustered_dummy' => 0,
        ),
      ),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_mouseposition' => array(
        'prefix' => '',
        'separator' => ', ',
        'suffix' => '',
        'numDigits' => '',
        'emptyString' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 0,
        'documentDrag' => 0,
      ),
      'openlayers_plus_behavior_blockswitcher' => array(
        'enabled' => 0,
        'open' => 0,
        'overlay_style' => 'radio',
        'position' => 'ne',
      ),
      'openlayers_plus_behavior_maptext' => array(
        'regions' => array(
          'draggable' => 0,
          'page_top' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'page_bottom' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'content' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'user_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'user_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'branding' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'menu' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'sidebar_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'sidebar_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'header_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'header_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'preface_third' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'postscript_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'postscript_second' => array(
            'name' => 1,
            'fieldset' => array(
              'toggle' => 1,
              'title' => 'Collapse',
              'collapse' => 0,
            ),
          ),
          'postscript_third' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'postscript_fourth' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'footer_first' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
          'footer_second' => array(
            'name' => 0,
            'fieldset' => array(
              'toggle' => 0,
              'title' => '',
              'collapse' => 0,
            ),
          ),
        ),
      ),
      'openlayers_plus_behavior_permalink' => array(),
      'openlayers_behavior_scaleline' => array(),
      'openlayers_behavior_zoompanel' => array(),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => array(
          'openaidmap_locations_clustered_amount' => 'openaidmap_locations_clustered_amount',
          'mapbox_natural' => 0,
          'openaidmap_locations_clustered_dummy' => 0,
          'openaidmap_locations_clustered_value' => 0,
        ),
        'point_zoom_level' => '8',
        'zoomtolayer_scale' => '1',
      ),
    ),
    'default_layer' => 'mapbox_natural',
    'layers' => array(
      'mapbox_natural' => 'mapbox_natural',
      'openaidmap_locations_clustered_dummy' => 'openaidmap_locations_clustered_dummy',
      'openaidmap_locations_clustered_amount' => 'openaidmap_locations_clustered_amount',
      'openaidmap_locations_clustered_value' => 'openaidmap_locations_clustered_value',
    ),
    'layer_weight' => array(
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_locations_clustered_dummy' => '0',
      'openaidmap_locations_clustered_amount' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_activities_overview_openlayers_1' => '0',
      'openlayers_kml_example' => '0',
      'geofield_formatter' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_locations_clustered_value' => '3',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_locations_clustered_dummy' => 'invisible',
      'openaidmap_locations_clustered_amount' => 'openaidmap_clustered',
      'openaidmap_locations_clustered_value' => 'openaidmap_clustered',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_locations_clustered_dummy' => 'invisible',
      'openaidmap_locations_clustered_amount' => 'openaidmap_clustered_selected',
      'openaidmap_locations_clustered_value' => 'openaidmap_clustered_selected',
    ),
    'layer_styles_temporary' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'openaidmap_activities_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_1' => '0',
      'openaidmap_activities_overview_openlayers_2' => '0',
      'openaidmap_organisations_overview_openlayers_1' => '0',
      'openaidmap_locations_clustered_dummy' => 'invisible',
      'openaidmap_locations_clustered_amount' => 'openaidmap_clustered_temporary',
      'openaidmap_locations_clustered_value' => 'openaidmap_clustered_temporary',
    ),
    'layer_activated' => array(
      'openaidmap_locations_clustered_dummy' => 'openaidmap_locations_clustered_dummy',
      'openaidmap_locations_clustered_amount' => 'openaidmap_locations_clustered_amount',
      'openaidmap_locations_clustered_value' => 0,
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'openaidmap_activities_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_2' => 0,
      'openaidmap_organisations_overview_openlayers_1' => 0,
    ),
    'layer_switcher' => array(
      'openaidmap_locations_clustered_amount' => 'openaidmap_locations_clustered_amount',
      'openaidmap_locations_clustered_value' => 'openaidmap_locations_clustered_value',
      'openaidmap_locations_clustered_dummy' => 0,
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'openaidmap_activities_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_1' => 0,
      'openaidmap_activities_overview_openlayers_2' => 0,
      'openaidmap_organisations_overview_openlayers_1' => 0,
    ),
    'projection' => 'EPSG:900913',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default_select',
      'temporary' => 'default',
    ),
  );
  $export['openaidmap_locations_map'] = $openlayers_maps;

  return $export;
}
