<?php
/**
 * @file
 * openaidmap_metadata_feature.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function openaidmap_metadata_feature_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_rdftype_iati_organisation';
  $strongarm->value = 'sioc:Item, foaf:Document';
  $export['rdf_rdftype_iati_organisation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_title_datatype_iati_organisation';
  $strongarm->value = 'iati:organisation';
  $export['rdf_title_datatype_iati_organisation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_title_predicates_iati_organisation';
  $strongarm->value = 'schema:name, rdf:type, ';
  $export['rdf_title_predicates_iati_organisation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rdf_title_type_iati_organisation';
  $strongarm->value = 'property';
  $export['rdf_title_type_iati_organisation'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'schemaorg_ui_type_iati_organisation';
  $strongarm->value = 'Organization';
  $export['schemaorg_ui_type_iati_organisation'] = $strongarm;

  return $export;
}
