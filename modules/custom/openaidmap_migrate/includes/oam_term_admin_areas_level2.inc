<?php

class OAMTermAdminAreasLevel2 extends DynamicMigration {
  public function __construct($arguments) {
    $this->arguments = $arguments;
    parent::__construct();

    // Do some general administration
    $this->description = t('Imports Admin Boundaries on the second sub-national level.');
//    $this->dependencies = array('OAMTermAdminAreasLevel1');

    // The definition of the columns. Keys are integers,
    // values are an array of field name then description.
    $csvcolumns = openaidmap_migrate_get_columns();
    $options = array();
    $fields = array(
      'parent' => 'parent',
    );
    // Instantiate the source class using the path to the CSV
    // file and the columns.
    $this->source = new MigrateSourceCSV($arguments['source_file'], $csvcolumns, $options, $fields);

    // Instantiate the destination class using the node type.
    $this->destination = new MigrateDestinationTerm('iati_admin_boundaries');

    // Instantiate the map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ter_provincia_codigo' => array(
          'type' => 'varchar',
          'length' => 16,
          'not null' => TRUE,
          'description' => 'ter_provincia_codigo',
          'alias' => 'tpc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Instantiate the field mapping.
    $this->addFieldMapping('name', 'ter_provincia');
    $this->addFieldMapping('description', 'ter_provincia_abrev');
    $this->addFieldMapping('parent', 'parent');
  }

  /**
   * Construct the machine name from the source file name.
   */
  protected function generateMachineName($class_name = NULL) {
    return 'OAMTermAdminAreasLevel2' . drupal_strtolower(pathinfo($this->arguments['source_file'], PATHINFO_FILENAME));
  }

  public function prepareRow($row) {
    $term = db_select(str_replace('2', '1', $this->getMap()->getMapTable()), 'mt')
      ->fields('mt', array('destid1'))
      ->condition('sourceid1', $row->ter_departamento_codigo)
      ->execute()
      ->fetchAssoc();
    if ($term === FALSE) {
      drupal_set_message('Could not find the first sub-country level of Admin Areas. Please load them before loading the second sub-national level of Admin Areas.');
      return FALSE;
    }
    $row->parent = $term['destid1'];
  }
}
